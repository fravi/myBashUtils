FROM quay.io/fravi/env-fatubuntu1604

ADD src /myBashUtils

WORKDIR /myBashUtils

ENTRYPOINT ["/bin/bash"]