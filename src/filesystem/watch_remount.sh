#!/usr/bin/env bash

echo <<EOF
# DESCRIPTION
this script will execute  --mount-command untill --path is a mounted dir,
then will watch to --path and if get unmounted, will attempt to remount it!

EOF

export PATH=$PATH":/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin"
export DST_PATH=""
export MOUNT_COMMAND=""
export PAUSE_RETRY="5"
export MAX_ITERATIONS="-1"


while [[ $# > 0 ]]
do
key="$1"

case $key in
    #######################        multiple args params
    --path)
    export DST_PATH="$2"
    shift
    ;;

    --mount-command)
    export MOUNT_COMMAND="$2"
    shift
    ;;

    --pause-retry)
    export PAUSE="$2"
    shift
    ;;

    --max-iterations)
    export MAX_ITERATIONS="$2"
    shift
    ;;

    *)  # unknown option
    ;;
esac
shift
done


print_help() {
  echo $0' --path [MOUNTED PATH]'
  echo '  ----mount-command [MOUNT COMMAND]'
  echo ''
  echo "OPTIONAL PARAMS: "
  echo "  --pause-retry / how long pause before retry mount , default=5 "
  echo "  --max-iterations / how many attempts to mount ( -1 = unlimited) , default=-1 "
}

validationExit(){
  print_help
  exit 1
}

validate_execution_params() {
  [ -z "${DST_PATH}" ] && {  echo 'DST_PATH not provided ' ; validationExit ; } || true
  [ -z "${MOUNT_COMMAND}" ] && { echo 'MOUNT_COMMAND not provided ' ; validationExit ; } || true
}


is_mounted(){
  if mount | grep -q " $1 "; then
    return 0 ; # true
  else
    return 1 ; # false
  fi
}

mount_loop(){
  ITER=0;
 while [ "$MAX_ITERATIONS" -eq "-1" -o "$MAX_ITERATIONS" -gt  "$ITER" ]  ; do

    if ! is_mounted "$DST_PATH" ; then

      echo "[mount_loop] attempt count $ITER / $MAX_ITERATIONS / DST_PATH : $DST_PATH"
      MNT_RESULTS=$($MOUNT_COMMAND)
      echo "[mount_loop] mount results = $MNT_RESULTS"

    else

      return 0 ; # true

    fi

    ITER=$((ITER+1))

    [ "$ITER" -gt "0" ] && /bin/sleep $PAUSE_RETRY
 done
 return is_mounted "$DST_PATH" ; # false
}


failed(){
  echo $1
  exit 1
}

watch_mount(){
  ACTION=$2
  mount_loop || failed "[failed] at start, mountloop error ?"
  while /usr/bin/inotifywait -e unmount "$DST_PATH"; do
      mount_loop || failed "[failed] after inotifywait"
  done
}


## PROGRAM EXECUTION START HERE

validate_execution_params
watch_mount