#!/usr/bin/env bash
set -x

echo "TEST TO BE IMRPOVED , please finish before using it "
echo "TODO: mount from block device while testing && test everything from a docker image"
exit 1 ;

CURRENT_DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
# USAGE
# export MC="mount command"
# export UMC="umount command"
# export WATCH_PATH="watch path"

export SUT="${CURRENT_DIR}/watch_remount.sh"

function validate(){
  [ -z "$MC" ] && {
    echo 'please export mount command into MC variable ie :  export MC="mount.cifs -o user=xxxxxxxx,pass=xxxxxxxxx //xxxxxxx.your-storagebox.de/backup /tmp/bk"'
    exit 1 ;
  }

  [ -z "$UMC" ] && {
    echo 'please export unmount command into UMC variable ie :  export UMC="umount /tmp/bk" '
    exit 1 ;
  }

  [ -z "$WATCH_PATH" ] && {
    echo 'please export WATCH_PATH variable ie :  export WATCH_PATH="/tmp/bk" '
    exit 1 ;
  }
}


function test_watch(){
  EXECUTIONS=${EXECUTIONS:-10}
  SUT_CMD=$SUT' --path "'$WATCH_PATH'" --mount-command "'$MC'" --max-iterations 3'
  for ((i = 0; i<=$EXECUTIONS ; i++)); do
    $SUT_CMD &
    BACKGRUOUND_WATCH=$!
    echo " test sleep 2 "
    sleep 2
    echo "now unmount and watch it fail"
    umount $WATCH_PATH

    wait $BACKGRUOUND_WATCH
  done



}


validate
test_watch

